# include <time.h>

# include "iGraphics.h"
# include "behaviour.h"
# include "checkmate.h"

// Defines //////////////////////////////////////////////////////////////

#define GAME_INIT 0
#define GAME_MENU 1
#define GAME_INS  2
#define GAME_RUN  3
#define GAME_EXIT 4
#define GAME_RE   5

#define CHECK_MATE 0

#define BLR "Black R.bmp"
#define BLN "Black N.bmp"
#define BLB "Black B.bmp"
#define BLQ "Black Q.bmp"
#define BLK "Black K.bmp"
#define BLP "Black P.bmp"

#define BRR "Brown R.bmp"
#define BRN "Brown N.bmp"
#define BRB "Brown B.bmp"
#define BRQ "Brown Q.bmp"
#define BRK "Brown K.bmp"
#define BRP "Brown P.bmp"

// FLAGS ////////////////////////////////////////////////////////////////

int mb_flag = 0;    //checks if lmb is pressed or not
int circle_flag = 0; //checks if there is a piece in the selected region
int set_flag = 0; //checks if the previous selection contained a piece or not
int player_flag = 0; //checks if the players are taking turns
int brp_flag = 0; //brown pawn promotion flag
int blp_flag = 0; //black pawn promotion flag
int blcheck_flag = 0; //black king in check
int brcheck_flag = 0; //brown king in check
int brcastling_flag0 = 1; //brown castling flag (Default = Enabled)
int blcastling_flag0 = 1; //black castling flag (Default = Enabled)
int brcastling_flag7 = 1; //black castling flag (Default = Enabled)
int blcastling_flag7 = 1; //brown castling flag (Default = Enabled)]
int checkmate_flag = 0; //checkmate flag
int time_flag1 = 0; //time flag1
int time_flag2 = 0; //time flag2
int prev_time = 0; //timer flag 
int menu_flag = -1; //menu selection flag


// Co - ordinates //////////////////////////////////////////////////////

int cur_x = 0, cur_y = 0;  //current coordinates
int prev_x = 0, prev_y = 0; //previous coordinates
int init_x = 450, init_y = 170; // initial coordinate of the piece
int check_x, check_y; //checked king position
int attack_x = -1, attack_y = -1; //attacking piece co ordinates

// Board - Arrays///////////////////////////////////////////////////////

int chess[8][8];
int black_stash[2][8];
int brown_stash[2][8];
int brown_piece[4] = {0,1,2,3};
int black_piece[4] = {9,10,11,12};
 
// Misc Declarations //////////////////////////////////////////////////

int game_state = GAME_INIT;
int bls_count = 0;
int brs_count = 0;
int debug = 0;
int interruption, kingcapture, escape, capture;
double start = time(NULL);
int menu_index = 0;
struct tm *time_dest, *time_cur;
char time_str[1000];
int br_score = 0;
int bl_score = 0;
char score[200];

// Board Resetting ////////////////////////////////////////////////// 

//=====================================================================================
void boardclear(void)
{
	int i, j;
	for(i = 0; i < 8; i++)
		for(j = 0; j < 8; j++)
		{
			if(chess[i][j] == -2) chess[i][j] = -1;
			if(chess[i][j] >= 100) chess[i][j] -= 100;
		}
}

void Time(void)
{
	int time_cur;

	time_cur = time(NULL);

	if(prev_time == 0)
	{
		prev_time = time(NULL);
		prev_time += 180;
	}

	if(prev_time - time_cur <= 0)
	{
		game_state = GAME_RE;
	}

	sprintf(time_str,"%0.2d : %0.2d",((prev_time-time_cur)/60)%60,((prev_time-time_cur)%60));
}


void MasterReset(void)
{
	int i, j;
	for(i = 0; i < 8; i++)
	{
		for(j = 0; j < 8; j++)
		{
			if(i == 0)
			{
				if(j <= 4) chess[i][j] = j;
				else chess[i][j] = 7-j;
			}
			else if(i == 1) chess[i][j] = 8;
			else if(i == 7)
			{
				if(j <= 4) chess[i][j] = 9+j;
				else chess[i][j] = 9+(7-j);
			}
			else if(i == 6) chess[i][j] = 17;
			else chess[i][j] = -1;
		}
	}
	for(i = 0; i < 2; i++)
	{
		for(j = 0; j < 8; j++)
		{
			black_stash[i][j] = -1;
			brown_stash[i][j] = -1;
		}
	}
	mb_flag = 0;    
	circle_flag = 0;
	set_flag = 0; 
	player_flag = 0; 
	brp_flag = 0;
	blp_flag = 0;
	brs_count = 0;
	bls_count = 0;
	brcheck_flag = 0;
	blcheck_flag = 0;
	brcastling_flag0 = 1; 
	blcastling_flag0 = 1; 
	brcastling_flag7 = 1; 
	blcastling_flag7 = 1; 
	checkmate_flag = 0; 
	time_flag1 = 0; 
	time_flag2 = 0; 
	prev_time = 0;  
	menu_flag = -1; 
}

void state(void)
{
	if(game_state == GAME_INIT) game_state = GAME_MENU;
	else if(game_state == GAME_MENU) game_state = GAME_RUN;
	else if(game_state == GAME_RUN) game_state = GAME_RE;
}

void undo(void)
{
	int temp;

	temp = chess[cur_y][cur_x];
	chess[cur_y][cur_x] = chess[prev_y][prev_x];
	chess[prev_y][prev_x] = temp;
	player_flag = 1 - player_flag;
}

//=====================================================================================

// Time Stopping
void StopTime(double tm)
{
    double ending, i = 0;
    ending = time(NULL);
	if(difftime(ending,start) == tm) 
	{
		state();
		time_flag1 = 0;
	}
}

//=====================================================================================
int stashcheck(int x)
{
	int i, j;
	for(i = 0; i < 2; i++)
		for(j = 0; j < 8; j++)
		{
			if(x == 0 && black_stash[i][j] >= 0) return 1;
			else if(x == 1 && brown_stash[i][j] >= 0) return 1;
		}
	return 0;
}
//=====================================================================================



// S A V E 

void SaveGame(void)
{
	FILE *fp;
	int i, j;

	fp = fopen("save1.txt","w");

	//flag saving
	fprintf(fp,"%d ",mb_flag);
	fprintf(fp,"%d ",circle_flag);
	fprintf(fp,"%d ",set_flag);
	fprintf(fp,"%d ",player_flag);
	fprintf(fp,"%d ",brp_flag);
	fprintf(fp,"%d ",blp_flag);
	fprintf(fp,"%d ",blcheck_flag);
	fprintf(fp,"%d ",brcheck_flag);
	fprintf(fp,"%d ",brcastling_flag0);
	fprintf(fp,"%d ",blcastling_flag0);
	fprintf(fp,"%d ",brcastling_flag7);
	fprintf(fp,"%d ",blcastling_flag7); 
	fprintf(fp,"%d ",checkmate_flag);
	fprintf(fp,"%d ",time_flag1);
	fprintf(fp,"%d ",time_flag2);

	//boardsaving
	for(i = 0; i < 8; i++)
	{
		for(j = 0; j < 8; j++)
		{
			fprintf(fp,"%d ",chess[i][j]);
		}
	}
	for(i = 0; i < 2; i++)
		for(j = 0; j < 8; j++)
		{
			fprintf(fp,"%d ",black_stash[i][j]);
		}
	for(i = 0; i < 2; i++)
		for(j = 0; j < 8; j++)
		{
			fprintf(fp,"%d",brown_stash[i][j]);
		}
	fclose(fp);
}

void LoadGame(void)
{
	FILE *fp;
	int i,j;

	fp = fopen("save1.txt","r");

	fscanf(fp,"%d",&mb_flag);
	fscanf(fp,"%d",&circle_flag);
	fscanf(fp,"%d",&set_flag);
	fscanf(fp,"%d",&player_flag);
	fscanf(fp,"%d",&brp_flag);
	fscanf(fp,"%d",&blp_flag);
	fscanf(fp,"%d",&blcheck_flag);
	fscanf(fp,"%d",&brcheck_flag);
	fscanf(fp,"%d",&brcastling_flag0);
	fscanf(fp,"%d",&blcastling_flag0);
	fscanf(fp,"%d",&brcastling_flag7);
	fscanf(fp,"%d",&blcastling_flag7);
	fscanf(fp,"%d",&checkmate_flag);
	fscanf(fp,"%d",&time_flag1);
	fscanf(fp,"%d",&time_flag2);

	for(i = 0; i < 8; i++)
		for(j = 0; j < 8; j++)
			fscanf(fp,"%d",&chess[i][j]);

	fclose(fp);
}
//=====================================================================================
// M E N U    B O A R D
void menu(int x)
{
	int i, j, flag = 0;
	for(i = 600; i < 760; i += 20)
	{
		flag = 1 - flag;
		for(j = 304; j < 464; j += 20)
		{
			if(flag == 0)
			{
				iSetcolor(1,0.92156,0.80392);
				iFilledRectangle(i,j,20,20);
				flag = 1 - flag;
			}
			else
			{
				iSetcolor(0.80392,0.52156,0.24705);
				iFilledRectangle(i,j,20,20);
				flag = 1 - flag;
			}
		}
	}
	if(x == 0)
	{
		MasterReset();
		for(i = 0; i < 8; i++)
		{
			for(j = 0; j < 8; j++)
			{
				if(chess[i][j] >= 0)
				{
					iSetcolor(0,0,0);
					iRectangle(600+20*j,304+20*i,20,20);
					iSetcolor(chess[i][j]/9?1:0,0,chess[i][j]/9?0:1);
					iLine(600+20*j,304+20*i,620+20*j,324+20*i);
					iLine(600+20*j,324+20*i,620+20*j,304+20*i);
				}
			}
		}
	}

	if(x == 1)
	{
		LoadGame();
		for(i = 0; i < 8; i++)
		{
			for(j = 0; j < 8; j++)
			{
				if(chess[i][j] >= 0)
				{
					iSetcolor(0,0,0);
					iRectangle(600+20*j,304+20*i,20,20);
					iSetcolor(chess[i][j]/9?1:0,0,chess[i][j]/9?0:1);
					iLine(600+20*j,304+20*i,620+20*j,324+20*i);
					iLine(600+20*j,324+20*i,620+20*j,304+20*i);
				}
			}
		}
	}
}


// C H E C K
void check(int x, int i, int j)
{
	int k, l;
	x = 1-x;
	brcheck_flag = 0;
	blcheck_flag = 0;
	behaviour(chess[i][j]%9,i,j,(int *) chess);
	for(k = 0; k < 8; k++)
		for(l = 0; l < 8; l++)
		{
			if(floor(chess[k][l]/9.0) != x && chess[k][l] >= 0)
			{
				if(chess[k][l] == 104) 
				{
					brcheck_flag = 1;
					check_x = k;
					check_y = l;
				}
				else if(chess[k][l] == 113)
				{
					blcheck_flag = 1;
					check_x = k;
					check_y = l;
				}
			}
		}
	if(brcheck_flag == 0 && blcheck_flag == 0)
	{
		attack_x = -1;
		attack_y = -1;
	}
	boardclear();
}

int check2(int x)
{
	 int i, j, k, l;

	 for(i = 0; i < 8; i++)
	 {
		 for(j = 0; j < 8; j++)
		 {
			 if(chess[i][j]/9 != x)
			 {
				//printf("%d\n",chess[i][j]/9);
				behaviour(chess[i][j]%9,i,j,(int *)chess);
				for(k = 0; k < 8; k++)
				{
					for(l = 0; l < 8; l++)
					{
						if(chess[k][l] == 104) return 1;
						else if(chess[k][l] == 113) return 1;
					}
				}
			 }
			 boardclear();
		 }
	 }
	 return 0;
}

//=====================================================================================

// C A S T L I N G
void Castling(int king_x, int king_y, int rook_x, int rook_y)
{
	int i = rook_x, j = rook_y;
	if(rook_y < king_y)
	{
		j = rook_y+1;
		while(j != king_y)
		{
			if(chess[i][j] != -1) break;
			j++;
		}
		if(j == king_y-1)
		{
			chess[king_x][king_y-2] = chess[king_x][king_y];
			chess[king_x][king_y] = -1;
			chess[rook_x][rook_y+3] = chess[rook_x][rook_y];
			chess[rook_x][rook_y] = -1;
			player_flag = 1-player_flag;
			if(chess[king_x][king_y+2] == 4)
			{
				brcastling_flag0 = 0;
				brcastling_flag7 = 0;
			}
			else
			{
				blcastling_flag0 = 0;
				brcastling_flag7 = 0;
			}
		}

	}
	else if(rook_y > king_y)
	{
		j = rook_y-1;
		while(j != king_y)
		{
			if(chess[i][j] != -1) break;
			j--;
		}
		if(king_y == j-1)
		{
			chess[king_x][king_y+2] = chess[king_x][king_y];
			chess[king_x][king_y] = -1;
			chess[rook_x][rook_y-2] = chess[rook_x][rook_y];
			chess[rook_x][rook_y] = -1;
			player_flag = 1-player_flag;
			if(chess[king_x][king_y+2] == 4)
			{
				brcastling_flag0 = 0;
				brcastling_flag7 = 0;
			}
			else
			{
				blcastling_flag0 = 0;
				brcastling_flag7 = 0;
			}
		}
	}
}
//=====================================================================================

//PAWN PROMOTION
void PawnPromotion(void)
{
	if((chess[cur_y][cur_x] == 8 && cur_y == 7) || brp_flag == 2)
	{
		if(brp_flag == 0) 
		{
			brp_flag = 1;
			prev_y = cur_y;
			prev_x = cur_x;
			player_flag = 1-player_flag;
		}
		if(brp_flag == 2)
		{
			chess[prev_y][prev_x] = brown_piece[cur_x];
			brp_flag = 0;
			player_flag = 1-player_flag;
		}
	}
	else if((chess[cur_y][cur_x] == 17 && cur_y == 0) || blp_flag == 2)
	{
		if(blp_flag == 0) 
		{
			blp_flag = 1;
			prev_y = cur_y;
			prev_x = cur_x;
			player_flag = 1-player_flag;
		}
		if(blp_flag == 2)
		{
			chess[prev_y][prev_x] = black_piece[cur_x];
			blp_flag = 0;
			player_flag = 1-player_flag;
		}
	}
}

//=====================================================================================

// Moving the pieces
void PieceMove(void)
{
	int temp, i, j;
	set_flag = 0;
	if(chess[cur_y][cur_x] == -2)
	{
		temp = chess[cur_y][cur_x];
		chess[cur_y][cur_x] = chess[prev_y][prev_x];
		chess[prev_y][prev_x] = temp;
		if(check2(player_flag) == 1) undo();
		circle_flag = 0;
		player_flag = 1 - player_flag;
		boardclear();
	}
	else if(chess[cur_y][cur_x] >= 100)
	{
		temp = chess[cur_y][cur_x];
		chess[cur_y][cur_x] = chess[prev_y][prev_x];
		chess[prev_y][prev_x] = -1;
		temp -= 100;
		if(floor(chess[cur_y][cur_x]/9.0) == 0)
		{
			i = (int)floor(brs_count/8.0);
			j = brs_count%8;
			brown_stash[i][j] = temp;
			brs_count++;
		}
		else if(floor(chess[cur_y][cur_x]/9.0) == 1)
		{
			i = (int)floor(bls_count/8.0);
			j = bls_count%8;
			black_stash[i][j] = temp;
			bls_count++;
		}
		circle_flag = 0;
		player_flag = 1 - player_flag;
		boardclear();
	}
	
	if(chess[cur_y][cur_x]%9 == 4)
	{
		if(chess[cur_y][cur_x] == 4) 
		{
			brcastling_flag0 = 0;
			brcastling_flag7 = 0;
		}
		else 
		{
			blcastling_flag0 = 0;
			blcastling_flag7 = 0;
		}
	}
	else if(chess[cur_y][cur_x]%9 == 0)
	{
		if(chess[cur_y][cur_x] == 0) brcastling_flag0 = 7;
		else blcastling_flag0 = 0;
	}
	else if(chess[cur_y][cur_x]%9 == 7)
	{
		if(chess[cur_y][cur_x] == 7) brcastling_flag7 = 0;
		else blcastling_flag7 = 0;
	}
	if(attack_x == -1 && attack_y == -1)
	{
		attack_x = cur_y;
		attack_y = cur_x;
	}
	if(attack_x > -1 && attack_y > -1) check(player_flag,attack_x,attack_y);

	//CHECKMATE
	if(blcheck_flag == 1 || brcheck_flag == 1)
	{
		interruption = Interruption(cur_y,cur_x,(int *)chess,check_x,check_y);
		escape = EscapeRoute(check_x,check_y,(int*)chess);
		capture = Capture(cur_y,cur_x,(int*)chess,check_x,check_y);
		kingcapture = KingCapture(check_x,check_y,(int*)chess,cur_y,cur_x);
		if(interruption+escape+capture+kingcapture == 0) checkmate_flag = 1;
	}

	//Checks and promotes a pawn
	PawnPromotion();

	prev_time = 0;
}

//=====================================================================================

/* 
	function iDraw() is called again and again by the system.
*/
void iDraw()
{
	int i, j, height = 40, width = 40, flag = 0, x;

	if(game_state == GAME_INIT)
	{
		if(time_flag1 == 0) 
		{
			start = time(NULL);
			time_flag1 = 1;
		}
		iShowBMP(0,0,"iChess4.bmp");
		StopTime(6.0);
	}

	else if(game_state == GAME_MENU)
	{
		//MENU
		iClear();
		iShowBMP(0,0,"iChessMenu2.bmp");
		iShowBMP(160,234,"BG.bmp");
		iShowBMP(1100,304,"menuscroll.bmp");
		menu(menu_index);
		switch(menu_index)
		{
		case 0:
			iShowBMP(170,440,"newgame2.bmp");
			iSetcolor(1,1,1);
			iShowBMP(190,380,"LOADgame.bmp");
			iShowBMP(190,320,"ins.bmp");
			iShowBMP(190,260,"EXIT.bmp");
			iShowBMP(800,260,"NEW.bmp");
			break;
		case 1:
			iSetcolor(0,1,0);
			iShowBMP(170,380,"LOADgame2.bmp");
			iSetcolor(1,1,1);
			iShowBMP(190,440,"newgame.bmp");
			iShowBMP(190,320,"ins.bmp");
			iShowBMP(190,260,"EXIT.bmp");
			iShowBMP(800,260,"LOAD.bmp");
			break;

		case 2:
			iShowBMP(170,320,"ins2.bmp");
			iShowBMP(190,440,"newgame.bmp");
			iShowBMP(190,380,"LOADgame.bmp");
			iShowBMP(190,260,"EXIT.bmp");
			iShowBMP(800,304,"INSTRUCTION.bmp");
			break;

		case 3:
			iSetcolor(0,1,0);
			iShowBMP(170,260,"EXIT2.bmp");
			iSetcolor(1,1,1);
			iShowBMP(190,440,"newgame.bmp");
			iShowBMP(190,380,"LOADgame.bmp");
			iShowBMP(190,320,"ins.bmp");
			iShowBMP(800,260,"EXITGM.bmp");
			break;
		}
	}

	else if(game_state == GAME_INS)
	{
		//INSTRUCTION MANUAL
		iSetcolor(0.690196,0.768627,0.870588);
		iFilledRectangle(0,0,1350,720);
		iSetcolor(0,0,0);
		iText(50,660,"I N  G A M E  C O N T R O L S",GLUT_BITMAP_HELVETICA_18);
		iText(50,620,"SELECTION...............[LMB]",GLUT_BITMAP_HELVETICA_18);
		iText(50,600,"SAVE GAME...............[ s ]",GLUT_BITMAP_HELVETICA_18);
		iText(50,580,"LOAD GAME...............[ l ]",GLUT_BITMAP_HELVETICA_18);
		iText(50,560,"QUICK EXIT..............[ q ]",GLUT_BITMAP_HELVETICA_18);
		iText(50,540,"RESET BOARD.............[ r ]",GLUT_BITMAP_HELVETICA_18);
		iText(50,500,"I N  M E N U  C O N T R O L S",GLUT_BITMAP_HELVETICA_18);
		iText(50,460,"UP......................[ w ]",GLUT_BITMAP_HELVETICA_18);
		iText(50,440,"DOWN....................[ s ]",GLUT_BITMAP_HELVETICA_18);
		iText(50,420,"SELECT..................[ENT]",GLUT_BITMAP_HELVETICA_18);
		iText(50,400,"POSITIVE................[ y ]",GLUT_BITMAP_HELVETICA_18);
		iText(50,380,"NEGATIVE................[ n ]",GLUT_BITMAP_HELVETICA_18);
		iText(50,220,"BACK TO MENU............[ <-]",GLUT_BITMAP_HELVETICA_18);
	}

	else if(game_state == GAME_RUN)
	{
		
		iClear(); // Creating the board

		//Making a Background
		iSetcolor(0.690196,0.768627,0.870588);
		iFilledRectangle(0,0,1360,768);
		iSetcolor(0,0,0);
		iRectangle(439,159,321,321);

		//Instruction List
		iSetcolor(0,0,0);
		iText(2,700,"[LMB] to select and move around",GLUT_BITMAP_HELVETICA_18);
		iText(2,680,"[r] to reset board",GLUT_BITMAP_HELVETICA_18);
		iText(2,660,"[q] to quit",GLUT_BITMAP_HELVETICA_18);

		
		//Who's turn is it
		if(player_flag == 0) 
		{
			iSetcolor(0.80392,0.52156,0.24705);
			if(brp_flag >= 1) iText(780,160,"PROMOTE YOUR PAWN",GLUT_BITMAP_HELVETICA_18);
			else iText(780,160,"Turn : Brown",GLUT_BITMAP_HELVETICA_18);
		}
		else 
		{
			
			iSetcolor(0,0,0);
			if(blp_flag >= 1) iText(780,460,"PROMOTE YOUR PAWN",GLUT_BITMAP_HELVETICA_18);
			else iText(780,460,"Turn : Black",GLUT_BITMAP_HELVETICA_18);
		}

		//Timer
		Time();
		iSetcolor(0,0,0);
		iText(1290,700,time_str,GLUT_BITMAP_HELVETICA_18);

		if(checkmate_flag == 1)
		{
			iText(440,700,"C   H   E    C    K    M    A    T    E",GLUT_BITMAP_HELVETICA_18);
			//printf("time : %d",time_flag);
			if(time_flag2 == 0)
			{
				start = time(NULL);
				time_flag2 = 1;
			}
			StopTime(3.0);
		}

		//Creating Brown Stash
		iSetcolor(1,0.894117,0.768627);
		iFilledRectangle(440,60,320,80);

		//Creating Black Stash
		iSetcolor(0.752941,0.752941,0.752941);
		iFilledRectangle(440,500,320,80);

		//Creating Brown Stash
		for(i = 440; i < 760; i += width)
		{
			iSetcolor(0,0,0);
			for(j = 60; j < 140; j += height) iRectangle(i,j,width,height);
			for(j = 500; j < 580; j += height) iRectangle(i,j,width,height);
		}

		//Brown Piece 
		iShowBMP(440+10,600+10,BRR);
		iShowBMP(480+10,600+10,BRN);
		iShowBMP(520+10,600+10,BRB);
		iShowBMP(560+10,600+10,BRQ);

		//Black Piece
		iShowBMP(440+10,20+10,BLR);
		iShowBMP(480+10,20+10,BLN);
		iShowBMP(520+10,20+10,BLB);
		iShowBMP(560+10,20+10,BLQ);

		for(i = 440; i < 760; i += width)
		{
			flag = 1 - flag;
			for(j = 160; j < 480; j += height)
			{
				if(flag == 0)
				{
					iSetcolor(1,0.92156,0.80392);
					iFilledRectangle(i,j,width,height);
					flag = 1 - flag;
				}
				else
				{
					iSetcolor(0.80392,0.52156,0.24705);
					iFilledRectangle(i,j,width,height);
					flag = 1 - flag;
				}
			}
		}

		// Setting up the pieces 
		for(i = 0; i < 8; i++)
		{
			for(j = 0; j < 8; j++)
			{
				switch(chess[i][j])
				{
				case 0:
				case 100:
					iShowBMP(440+(width*j)+10,160+(height*i)+10,BRR);
					if(chess[i][j] >= 100)
					{
						iSetcolor(0,0,0);
						iRectangle(442+(width*j),162+(height*i),38,38);
					}
					break;
				case 1:
				case 101:
					iShowBMP(440+(width*j)+10,160+(height*i)+10,BRN);
					if(chess[i][j] >= 100)
					{
						iSetcolor(0,0,0);
						iRectangle(442+(width*j),162+(height*i),38,38);
					}
					break;
				case 2:
				case 102:
					iShowBMP(440+(width*j)+10,160+(height*i)+10,BRB);
					if(chess[i][j] >= 100)
					{
						iSetcolor(0,0,0);
						iRectangle(442+(width*j),162+(height*i),38,38);
					}
					break;
				case 3:
				case 103:
					iShowBMP(440+(width*j)+10,160+(height*i)+10,BRQ);
					if(chess[i][j] >= 100)
					{
						iSetcolor(0,0,0);
						iRectangle(442+(width*j),162+(height*i),38,38);
					}
					break;
				case 4:
				case 104:
					iShowBMP(440+(width*j)+10,160+(height*i)+10,BRK);
					if(chess[i][j] >= 100)
					{
						iSetcolor(0,0,0);
						iRectangle(442+(width*j),162+(height*i),38,38);
					}
					if(brcheck_flag == 1)
					{
						iSetcolor(0,0,0);
						iCircle(460+(width*j),180+(height*i),20);
					}
					break;
				case 8:
				case 108:
					iShowBMP(440+(width*j)+10,160+(height*i)+10,BRP);
					if(chess[i][j] >= 100)
					{
						iSetcolor(0,0,0);
						iRectangle(442+(width*j),162+(height*i),38,38);
					}
					break;
				case 9:
				case 109:
					iShowBMP(440+(width*j)+10,160+(height*i)+10,BLR);
					if(chess[i][j] >= 100)
					{
						iSetcolor(0,0,0);
						iRectangle(442+(width*j),162+(height*i),38,38);
					}
					break;
				case 10:
				case 110:
					iShowBMP(440+(width*j)+10,160+(height*i)+10,BLN);
					if(chess[i][j] >= 100)
					{
						iSetcolor(0,0,0);
						iRectangle(442+(width*j),162+(height*i),38,38);
					}
					break;
				case 11:
				case 111:
					iShowBMP(440+(width*j)+10,160+(height*i)+10,BLB);
					if(chess[i][j] >= 100)
					{
						iSetcolor(0,0,0);
						iRectangle(442+(width*j),162+(height*i),38,38);
					}
					break;
				case 12:
				case 112:
					iShowBMP(440+(width*j)+10,160+(height*i)+10,BLQ);
					if(chess[i][j] >= 100)
					{
						iSetcolor(0,0,0);
						iRectangle(442+(width*j),162+(height*i),38,38);
					}
					break;
				case 13:
				case 113:
					iShowBMP(440+(width*j)+10,160+(height*i)+10,BLK);
					if(chess[i][j] >= 100)
					{
						iSetcolor(0,0,0);
						iRectangle(442+(width*j),162+(height*i),38,38);
					}
					if(blcheck_flag == 1)
					{
						iSetcolor(0,0,0);
						iCircle(460+(width*j),180+(height*i),20);
					}
					break;
				case 17:
				case 117:
					iShowBMP(440+(width*j)+10,160+(height*i)+10,BLP);
					if(chess[i][j] >= 100)
					{
						iSetcolor(0,0,0);
						iRectangle(442+(width*j),162+(height*i),38,38);
					}
					break;
				case -2:
					iSetcolor(0,0,1);
					iRectangle(442+(width*j),162+(height*i),38,38);
					break;
				} //end switch
			} //end for
		} //end for

		for(i = 0; i < 2; i++)
		{
			for(j = 0; j < 8; j++)
			{
				x = black_stash[i][j];
				switch(x)
				{
				case 0:
					iShowBMP(440+(width*j)+10,500+(height*i)+10,BRR);
					break;
				case 1:
					iShowBMP(440+(width*j)+10,500+(height*i)+10,BRN);
					break;
				case 2:
					iShowBMP(440+(width*j)+10,500+(height*i)+10,BRB);
					break;
				case 3:
					iShowBMP(440+(width*j)+10,500+(height*i)+10,BRQ);
					break;
				case 4:
					iShowBMP(440+(width*j)+10,500+(height*i)+10,BRK);
					break;
				case 8:
					iShowBMP(440+(width*j)+10,500+(height*i)+10,BRP);
					break;
				}
			}

			for(j = 0; j < 8; j++)
			{
				x = brown_stash[i][j]%9;
				switch(x)
				{
				case 0:
					iShowBMP(440+(width*j)+10,60+(height*i)+10,BLR);
					break;
				case 1:
					iShowBMP(440+(width*j)+10,60+(height*i)+10,BLN);
					break;
				case 2:
					iShowBMP(440+(width*j)+10,60+(height*i)+10,BLB);
					break;
				case 3:
					iShowBMP(440+(width*j)+10,60+(height*i)+10,BLQ);
					break;
				case 4:
					iShowBMP(440+(width*j)+10,60+(height*i)+10,BLK);
					break;
				case 8:
					iShowBMP(440+(width*j)+10,60+(height*i)+10,BLP);
					break;
				}
			}
		}

		if(mb_flag == 1)
		{
			iSetcolor(1,0,0);
			iRectangle(442+(width*cur_x),162+(height*cur_y),38,38);
		}
	}

	else if(game_state == GAME_RE)
	{
		iClear();
		iSetcolor(1,1,1);
		if(player_flag == 0)
		{
			iSetcolor(1,1,1);
			iFilledRectangle(0,0,1350,720);
			iShowBMP(100,50,"Black.bmp");
			iSetcolor(0,0,0);
			iText(900,632,"BLACK WINS",GLUT_BITMAP_HELVETICA_18);
			iShowBMP(1000,200,"RESTART.bmp");
		}
		else if(player_flag == 1)
		{
			iSetcolor(1,1,1);
			iFilledRectangle(0,0,1350,720);
			iShowBMP(100,50,"Brown.bmp");
			iSetcolor(0,0,0);
			iText(900,632,"BROWN WINS",GLUT_BITMAP_HELVETICA_18);
			iShowBMP(1000,200,"RESTART.bmp");
		}
	}
}// end iDraw


//=====================================================================================
/* 
	function iMouseMove() is called when the user presses and drags the mouse.
	(mx, my) is the position where the mouse pointer is.
*/
void iMouseMove(int mx, int my)
{
	//place your codes here
}

/* 
	function iMouse() is called when the user presses/releases the mouse.
	(mx, my) is the position where the mouse pointer is.
*/
void iMouse(int button, int state, int mx, int my)
{

	if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		if(game_state == GAME_MENU)
		{
			if(mx >= 190 && mx <= 490)
			{
				if(my >= 440 && my <= 475) 
				{
					game_state = GAME_RUN;
				}
				else if(my >= 380 && my <= 415) 
				{
					FILE *fp;

					fp = fopen("save1.txt","r");
					if(!feof(fp)) LoadGame();

					game_state = GAME_RUN;
				}
				else if(my >= 320 && my <= 355) game_state = GAME_INS;
				else if(my >= 260 && my <= 295) 
				{
					exit(0);
				}
			}
		}
		if(mx >= 440 && mx <= 760 && my >= 160 && my <= 480 && brp_flag == 0 && blp_flag == 0)
		{
			//locating the x co ordinate of the region
			if(440 <= mx && mx < 480) cur_x = 0;
			else if(480 <= mx && mx < 520) cur_x = 1;
			else if(520 <= mx && mx < 560) cur_x = 2;
			else if(560 <= mx && mx < 600) cur_x = 3;
			else if(600 <= mx && mx < 640) cur_x = 4;
			else if(640 <= mx && mx < 680) cur_x = 5;
			else if(680 <= mx && mx < 720) cur_x = 6;
			else if(720 <= mx && mx <= 760) cur_x = 7;

			//locating the y co ordinate of the region
			if(160 <= my && my < 200) cur_y = 0;
			else if(200 <= my && my < 240) cur_y = 1;
			else if(240 <= my && my < 280) cur_y = 2;
			else if(280 <= my && my < 320) cur_y = 3;
			else if(320 <= my && my < 360) cur_y = 4;
			else if(360 <= my && my < 400) cur_y = 5;
			else if(400 <= my && my < 440) cur_y = 6;
			else if(440 <= my && my <= 480) cur_y = 7;

			mb_flag = 1;

			if(floor(chess[cur_y][cur_x]/9.0) == floor(chess[prev_y][prev_x]/9.0) && circle_flag == 1)
			{
				if(brcastling_flag0 == 1 && chess[cur_y][cur_x] == 0 && chess[prev_y][prev_x] == 4) Castling(prev_y,prev_x,cur_y,cur_x);
				else if(brcastling_flag7 == 1 && chess[cur_y][cur_x] == 7 && chess[prev_y][prev_x] == 4) Castling(prev_y,prev_x,cur_y,cur_x);
				else if(blcastling_flag7 == 1 && chess[cur_y][cur_x] == 17 && chess[prev_y][prev_x] == 13) Castling(prev_y,prev_x,cur_y,cur_x);
				else if(blcastling_flag0 == 1 && chess[cur_y][cur_x] == 9 && chess[prev_y][prev_x] == 13) Castling(prev_y,prev_x,cur_y,cur_x);
				circle_flag = 0;
				set_flag = 0;
				boardclear();
			}

			if(chess[cur_y][cur_x] >= 0 && circle_flag == 0 && player_flag == floor(chess[cur_y][cur_x]/9.0)) 
			{
				circle_flag = 1;
				behaviour(chess[cur_y][cur_x]%9,cur_y,cur_x,(int *)chess);
				prev_x = cur_x;
				prev_y = cur_y;
			}
			else if(circle_flag == 1) set_flag = 1;
			if(set_flag == 1) PieceMove();
		}
		//locating mouse position if player takes piece from stash
		else if((brp_flag == 1 || blp_flag == 1) && 440 <= mx && mx <= 600)
		{
			if(440 <= mx && mx < 480) cur_x = 0;
			else if(480 <= mx && mx < 520) cur_x = 1;
			else if(520 <= mx && mx < 560) cur_x = 2;
			else if(560 <= mx && mx < 600) cur_x = 3;

			//co ordinate
			if(blp_flag == 1 && 20 <= my && my <= 60) 
			{
				blp_flag = 2;
				set_flag = 1;
			}
			else if(brp_flag == 1 && 600 <= my && my <= 640)
			{
				brp_flag = 2;
				set_flag = 1;
			}
			if(set_flag == 1) PieceMove();
		}
	}
	if(button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
	{
		//place your codes here	
		if(mb_flag == 1)
		{
			circle_flag = 0;
			mb_flag = 0;
			set_flag = 0;
			player_flag = 1 - player_flag;
			boardclear();
		}
	}
}


//=====================================================================================
/*
	function iKeyboard() is called whenever the user hits a key in keyboard.
	key- holds the ASCII value of the key pressed.	
*/
void iKeyboard(unsigned char key)
{
	if(game_state == GAME_RE)
	{
		if(key == 'y')
		{
			MasterReset();
			game_state = GAME_RUN;
		}
		if(key == 'n')
		{
			game_state = GAME_MENU;
		}
	}

	if(game_state == GAME_INS)
	{
		if(key == '\b') game_state = GAME_MENU;
	}

	if(key == 'q')
	{
		exit(0);
	}
	if(key == 'r')
	{
		MasterReset();
	}

	if(key == ' ' && game_state == GAME_INIT) 
	{
		game_state = GAME_MENU;
	}

	if(key == 's' && game_state == GAME_RUN) SaveGame();

	if(key == 'l') LoadGame();

	if(key == 't') Time();

	if(game_state == GAME_MENU)
	{
		if(key == 'w')
		{
			menu_index--;
			if(menu_index == -1) menu_index = 3;
			menu_index = menu_index%4;
		}

		if(key == 's')
		{
			menu_index++;
			menu_index %= 4;
		}
		
		if(key == '\r')
		{
			switch(menu_index)
			{
			case 0:
				game_state = GAME_RUN;
				break;
			case 1:
				FILE *fp;
				fp = fopen("save1.txt","r");
				if(feof(fp) != EOF) LoadGame();
				game_state = GAME_RUN;
				break;

			case 2:
				game_state = GAME_INS;
				break;

			case 3:
				exit(0);
				break;
			}
		}
	}
	//place your codes for other keys here
}


//=====================================================================================
int main()
{
	int i, j;

	if(game_state == GAME_INIT)
	{
		for(i = 0; i < 8; i++)
		{
			for(j = 0; j < 8; j++)
			{
				if(i == 0)
				{
					if(j <= 4) chess[i][j] = j;
					else chess[i][j] = 7-j;
				}
				else if(i == 1) chess[i][j] = 8;
				else if(i == 7)
				{
					if(j <= 4) chess[i][j] = 9+j;
					else chess[i][j] = 9+(7-j);
				}
				else if(i == 6) chess[i][j] = 17;
				else chess[i][j] = -1;
			}
		}
		for(i = 0; i < 2; i++)
		{
			for(j = 0; j < 8; j++)
			{
				black_stash[i][j] = -1;
				brown_stash[i][j] = -1;
			}
		}
	}

	iInitialize(1350,720,"iChess");
	return 0;
}	