#include <stdio.h>
#include <math.h>

void behaviour(int x, int pos_x, int pos_y, int* chess)
{
	int i = pos_x, j = pos_y;
	switch(x)
	{
	case 0:   //Rook
		i = pos_x+1;
		j = pos_y;
		while(i < 8)
		{
			if(chess[i*8+j] == -1) 
			{
				chess[i*8+j] = -2;
				i++;
			}
			else if(floor(chess[i*8+j]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[i*8+j] < 100 && chess[i*8+j] >= 0)
			{
				chess[i*8+j] += 100;
				break;
			}
			else break;
		}
		i = pos_x-1;
		while(i >= 0)
		{
			if(chess[i*8+j] == -1) 
			{
				chess[i*8+j] = -2;
				i--;
			}
			else if(floor(chess[i*8+j]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[i*8+j] < 100 && chess[i*8+j] >= 0)
			{
				chess[i*8+j] += 100;
				break;
			}
			else break;
		}
		i = pos_x;
		j = pos_y+1;
		while(j < 8)
		{
			if(chess[i*8+j] == -1) 
			{
				chess[i*8+j] = -2;
				j++;
			}
			else if(floor(chess[i*8+j]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[i*8+j] < 100 && chess[i*8+j] >= 0)
			{
				chess[i*8+j] += 100;
				break;
			}
			else break;
		}
		j = pos_y-1;
		while(j >= 0)
		{
			if(chess[i*8+j] == -1) 
			{
				chess[i*8+j] = -2;
				j--;
			}
			else if(floor(chess[i*8+j]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[i*8+j] < 100 && chess[i*8+j] >= 0)
			{
				chess[i*8+j] += 100;
				break;
			}
			else break;
		}
		break;
	
	case 1:  //Knight
		if(i+2 <= 7)
		{
			if(j+1 <= 7 && chess[(i+2)*8+j+1] == -1) chess[(i+2)*8+j+1] = -2;
			else if(j+1 <= 7 && chess[(i+2)*8+j+1] >= 0 && floor(chess[(i+2)*8+j+1]/9.0) != floor(chess[i*8+j]/9.0) && chess[(i+2)*8+j+1] < 100 && chess[(i+2)*8+j+1] >= 0) chess[(i+2)*8+j+1] += 100;
			if(j-1 >= 0 && chess[(i+2)*8+j-1] == -1) chess[(i+2)*8+j-1] = -2;
			else if(j-1 >= 0 && chess[(i+2)*8+j-1] >= 0 && floor(chess[(i+2)*8+j-1]/9.0) != floor(chess[i*8+j]/9.0) && chess[(i+2)*8+j-1] < 100 && chess[(i+2)*8+j-1] >= 0) chess[(i+2)*8+j-1] += 100;
		}
		if(i+1 <= 7)
		{
			if(j+2 <= 7 && chess[(i+1)*8+j+2] == -1) chess[(i+1)*8+j+2] = -2;
			else if(j+2 <= 7 && chess[(i+1)*8+j+2] >= 0 && floor(chess[(i+1)*8+j+2]/9.0) != floor(chess[i*8+j]/9.0) && chess[(i+1)*8+j+2] < 100 && chess[(i+1)*8+j+2] >= 0) chess[(i+1)*8+j+2] += 100;
			if(j-2 >= 0 && chess[(i+1)*8+j-2] == -1) chess[(i+1)*8+j-2] = -2;
			else if(j-2 >= 0 && chess[(i+1)*8+j-2] >= 0 && floor(chess[(i+1)*8+j-2]/9.0) != floor(chess[i*8+j]/9.0) && chess[(i+1)*8+j-2] < 100 && chess[(i+1)*8+j-2] >= 0) chess[(i+1)*8+j-2] += 100;
		}
		if(i-1 >= 0)
		{
			if(j+2 <= 7 && chess[(i-1)*8+j+2] == -1) chess[(i-1)*8+j+2] = -2;
			else if(j+2 <= 7 && chess[(i-1)*8+j+2] >= 0 && floor(chess[(i-1)*8+j+2]/9.0) != floor(chess[i*8+j]/9.0) && chess[(i-1)*8+j+2] < 100 && chess[(i-1)*8+j+2] >= 0) chess[(i-1)*8+j+2] += 100;
			if(j-2 >= 0 && chess[(i-1)*8+j-2] == -1) chess[(i-1)*8+j-2] = -2;
			else if(j-2 >= 0 && chess[(i-1)*8+j-2] >= 0 && floor(chess[(i-1)*8+j-2]/9.0) != floor(chess[i*8+j]/9.0) && chess[(i-1)*8+j-2] < 100 && chess[(i-1)*8+j-2] >= 0) chess[(i-1)*8+j-2] += 100;
		}
		if(i-2 >= 0)
		{
			if(j+1 <= 7 && chess[(i-2)*8+j+1] == -1) chess[(i-2)*8+j+1] = -2;
			else if(j+1 <= 7 && chess[(i-2)*8+j+1] >= 0 && floor(chess[(i-2)*8+j+1]/9.0) != floor(chess[i*8+j]/9.0) && chess[(i-2)*8+j+1] < 100 && chess[(i-2)*8+j+1] >= 0) chess[(i-2)*8+j+1] += 100;
			if(j-1 >= 0 && chess[(i-2)*8+j-1] == -1) chess[(i-2)*8+j-1] = -2;
			else if(j-1 >= 0 && chess[(i-2)*8+j-1] >= 0 && floor(chess[(i-2)*8+j-1]/9.0) != floor(chess[i*8+j]/9.0) && chess[(i-2)*8+j-1] < 100 && chess[(i-2)*8+j-1] >= 0) chess[(i-2)*8+j-1] += 100;
		}
		break;

	case 2: //Bishop
		i = pos_x+1;
		j = pos_y+1;
		while(i <= 7 && j <= 7)
		{
			if(chess[i*8+j] == -1)
			{
				chess[i*8+j] = -2;
				i++;
				j++;
			}
			else if(floor(chess[i*8+j]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[i*8+j] < 100 && chess[i*8+j] >= 0)
			{
				chess[i*8+j] += 100;
				break;
			}
			else break;
		}
		i = pos_x+1;
		j = pos_y-1;
		while(i <= 7 && j >= 0)
		{
			if(chess[i*8+j] == -1)
			{
				chess[i*8+j] = -2;
				i++;
				j--;
			}
			else if(floor(chess[i*8+j]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[i*8+j] < 100 && chess[i*8+j] >= 0)
			{
				chess[i*8+j] += 100;
				break;
			}
			else break;
		}
		i = pos_x-1;
		j = pos_y-1;
		while(i >= 0 && j >= 0)
		{
			if(chess[i*8+j] == -1)
			{
				chess[i*8+j] = -2;
				i--;
				j--;
			}
			else if(floor(chess[i*8+j]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[i*8+j] < 100 && chess[i*8+j] >= 0)
			{
				chess[i*8+j] += 100;
				break;
			}
			else break;
		}
		i = pos_x-1;
		j = pos_y+1;
		while(i >= 0 && j <= 7)
		{
			if(chess[i*8+j] == -1)
			{
				chess[i*8+j] = -2;
				i--;
				j++;
			}
			else if(floor(chess[i*8+j]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[i*8+j] < 100 && chess[i*8+j] >= 0)
			{
				chess[i*8+j] += 100;
				break;
			}
			else break;
		}
		break;

	case 3: //Queen
		//Bishop Properties
		i = pos_x+1;
		j = pos_y+1;
		while(i <= 7 && j <= 7)
		{
			if(chess[i*8+j] == -1)
			{
				chess[i*8+j] = -2;
				i++;
				j++;
			}
			else if(floor(chess[i*8+j]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[i*8+j] < 100 && chess[i*8+j] >= 0)
			{
				chess[i*8+j] += 100;
				break;
			}
			else break;
		}
		i = pos_x+1;
		j = pos_y-1;
		while(i <= 7 && j >= 0)
		{
			if(chess[i*8+j] == -1)
			{
				chess[i*8+j] = -2;
				i++;
				j--;
			}
			else if(floor(chess[i*8+j]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[i*8+j] < 100 && chess[i*8+j] >= 0)
			{
				chess[i*8+j] += 100;
				break;
			}
			else break;
		}
		i = pos_x-1;
		j = pos_y-1;
		while(i >= 0 && j >= 0)
		{
			if(chess[i*8+j] == -1)
			{
				chess[i*8+j] = -2;
				i--;
				j--;
			}
			else if(floor(chess[i*8+j]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[i*8+j] < 100 && chess[i*8+j] >= 0)
			{
				chess[i*8+j] += 100;
				break;
			}
			else break;
		}
		i = pos_x-1;
		j = pos_y+1;
		while(i >= 0 && j <= 7)
		{
			if(chess[i*8+j] == -1)
			{
				chess[i*8+j] = -2;
				i--;
				j++;
			}
			else if(floor(chess[i*8+j]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[i*8+j] < 100 && chess[i*8+j] >= 0)
			{
				chess[i*8+j] += 100;
				break;
			}
			else break;
		}

		//Rook Properties
		i = pos_x+1;
		j = pos_y;
		while(i < 8)
		{
			if(chess[i*8+j] == -1) 
			{
				chess[i*8+j] = -2;
				i++;
			}
			else if(floor(chess[i*8+j]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[i*8+j] < 100 && chess[i*8+j] >= 0)
			{
				chess[i*8+j] += 100;
				break;
			}
			else break;
		}
		i = pos_x-1;
		while(i >= 0)
		{
			if(chess[i*8+j] == -1) 
			{
				chess[i*8+j] = -2;
				i--;
			}
			else if(floor(chess[i*8+j]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[i*8+j] < 100 && chess[i*8+j] >= 0)
			{
				chess[i*8+j] += 100;
				break;
			}
			else break;
		}
		i = pos_x;
		j = pos_y+1;
		while(j < 8)
		{
			if(chess[i*8+j] == -1) 
			{
				chess[i*8+j] = -2;
				j++;
			}
			else if(floor(chess[i*8+j]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[i*8+j] < 100 && chess[i*8+j] >= 0)
			{
				chess[i*8+j] += 100;
				break;
			}
			else break;
		}
		j = pos_y-1;
		while(j >= 0)
		{
			if(chess[i*8+j] == -1) 
			{
				chess[i*8+j] = -2;
				j--;
			}
			else if(floor(chess[i*8+j]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[i*8+j] < 100 && chess[i*8+j] >= 0)
			{
				chess[i*8+j] += 100;
				break;
			}
			else break;
		}
		break;

	case 4: //King
		if(i+1 <= 7)
		{
			if(j+1 <= 7 && chess[(i+1)*8+j+1] == -1) chess[(i+1)*8+j+1] = -2;
			else if(j+1 <= 7 && floor(chess[(i+1)*8+j+1]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[(i+1)*8+j+1] < 100 && chess[(i+1)*8+j+1] >= 0) chess[(i+1)*8+j+1] += 100;
			if(j <= 7 && chess[(i+1)*8+j] == -1) chess[(i+1)*8+j] = -2;
			else if(j <= 7 && floor(chess[(i+1)*8+j]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[(i+1)*8+j] < 100 && chess[(i+1)*8+j] >= 0) chess[(i+1)*8+j] += 100;
			if(j-1 >= 0 && chess[(i+1)*8+j-1] == -1) chess[(i+1)*8+j-1] = -2;
			else if(j-1 >= 0 && floor(chess[(i+1)*8+j-1]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[(i+1)*8+j-1] < 100 && chess[(i+1)*8+j-1] >= 0) chess[(i+1)*8+j-1] += 100;
		}
		if(i >= 0 && i <= 7)
		{
			if(j+1 <= 7 && chess[i*8+j+1] == -1) chess[i*8+j+1] = -2;
			else if(j+1 <= 7 && floor(chess[i*8+j+1]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[i*8+j+1] < 100 && chess[i*8+j+1] >= 0) chess[i*8+j+1] += 100;
			if(j-1 >= 0 && chess[i*8+j-1] == -1) chess[i*8+j-1] = -2;
			else if(j-1 >= 0 && floor(chess[i*8+j-1]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[i*8+j-1] < 100 && chess[i*8+j-1] >= 0) chess[i*8+j-1] += 100;
		}
		if(i-1 >= 0)
		{
			if(j+1 <= 7 && chess[(i-1)*8+j+1] == -1) chess[(i-1)*8+j+1] = -2;
			else if(j+1 <= 7 && floor(chess[(i-1)*8+j+1]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[(i-1)*8+j+1] < 100 && chess[(i-1)*8+j+1] >= 0) chess[(i-1)*8+j+1] += 100;
			if(j <= 7 && chess[(i-1)*8+j] == -1) chess[(i-1)*8+j] = -2;
			else if(j <= 7 && floor(chess[(i-1)*8+j]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[(i-1)*8+j] < 100 && chess[(i-1)*8+j] >= 0) chess[(i-1)*8+j] += 100;
			if(j-1 >= 0 && chess[(i-1)*8+j-1] == -1) chess[(i-1)*8+j-1] = -2;
			else if(j-1 >= 0 && floor(chess[(i-1)*8+j-1]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[(i-1)*8+j-1] < 100 && chess[(i-1)*8+j-1] >= 0) chess[(i-1)*8+j-1] += 100;
		}
		break;

	case 8: //Pawn
		if(chess[i*8+j] == 8)
		{
			if(i+1 <= 7 && chess[(i+1)*8+j] == -1) 
			{
				chess[(i+1)*8+j] = -2;
				if(i+2 <= 7 && chess[(i+2)*8+j] == -1 && i == 1) chess[(i+2)*8+j] = -2;
			}
			if(i+1 <= 7)
			{
				if(j+1 <= 7 && chess[(i+1)*8+j+1] >= 0 && floor(chess[(i+1)*8+j+1]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[(i+1)*8+j+1] < 100 && chess[(i+1)*8+j+1] >= 0) chess[(i+1)*8+j+1] += 100;
				if(j-1 >= 0 && chess[(i+1)*8+j-1] >= 0 && floor(chess[(i+1)*8+j-1]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[(i+1)*8+j-1] < 100 && chess[(i+1)*8+j-1] >= 0) chess[(i+1)*8+j-1] += 100;
			}
		}
		else if(chess[i*8+j] == 17)
		{
			if(i-1 >= 0 && chess[(i-1)*8+j] == -1) 
			{
				chess[(i-1)*8+j] = -2;
				if(i-2 >= 0 && chess[(i-2)*8+j] == -1 && i == 6) chess[(i-2)*8+j] = -2;
			}
			if(i-1 >= 0)
			{
				if(j+1 <= 7 && chess[(i-1)*8+j+1] >= 0 && floor(chess[(i-1)*8+j+1]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[(i-1)*8+j+1] < 100 && chess[(i-1)*8+j+1] < 100) chess[(i-1)*8+j+1] += 100;
				if(j-1 >= 0 && chess[(i-1)*8+j-1] >= 0 && floor(chess[(i-1)*8+j-1]/9.0) != floor(chess[pos_x*8+pos_y]/9.0) && chess[(i-1)*8+j-1] < 100 && chess[(i-1)*8+j-1] < 100) chess[(i-1)*8+j-1] += 100;
			}
		}
		break;
	}
}
