#ifndef CHECKMATE_H_INCLUDE
#define CHECKMATE_H_INCLUDE

void BoardClear(int *chess);
int EscapeRoute(int king_x, int king_y, int *chess);
int Interruption(int attacking_x,int attacking_y, int *chess, int king_x, int king_y);
int Capture(int attacking_x, int attacking_y, int *chess, int king_x, int king_y);
int KingCapture(int king_x, int king_y, int *chess, int attacking_x, int attacking_y);

#endif