//Testing for the checkmate condition
#include <stdio.h>
#include <math.h>
#include "behaviour.h"

void BoardClear(int *chess)
{
	int i, j;
	for(i = 0; i < 8; i++)
		for(j = 0; j < 8; j++)
		{
			if(chess[i*8+j] == -2) chess[i*8+j] = -1;
			if(chess[i*8+j] >= 100) chess[i*8+j] -= 100;
		}
}


int EscapeRoute(int king_x, int king_y, int *chess)
{
	int i, j, escape_flag = 0;
	for(i = 0; i < 8; i++)
		for(j = 0; j < 8; j++)
		{
			if(floor(chess[i*8+j]/9.0) != floor(chess[king_x*8+king_y]%100/9.0))
			{
				behaviour(chess[i*8+j]%9,i,j,chess);
			}
		}
		if(king_x+1 <= 7)
		{
			if(king_y+1 <= 7 && chess[(king_x+1)*8+king_y+1] == -1) escape_flag = 1;
			if(king_y <= 7 && chess[(king_x+1)*8+king_y] == -1) escape_flag = 1;
			if(king_y-1 >= 0 && chess[(king_x+1)*8+king_y-1] == -1) escape_flag = 1;
		}
		if(king_x >= 0 && king_x <= 7)
		{
			if(king_y+1 <= 7 && chess[king_x*8+king_y+1] == -1) escape_flag = 1;
			if(king_y-1 >= 0 && chess[king_x*8+king_y-1] == -1) escape_flag = 1;
		}
		if(king_x-1 >= 0)
		{
			if(king_y+1 <= 7 && chess[(king_x-1)*8+king_y+1] == -1) escape_flag = 1;
			if(king_y <= 7 && chess[(king_x-1)*8+king_y] == -1) escape_flag = 1;
			if(king_y-1 >= 0 && chess[(king_x-1)*8+king_y-1] == -1) escape_flag = 1;
		}
	BoardClear(chess);
	return escape_flag;
}

int Interruption(int attacking_x,int attacking_y, int *chess, int king_x, int king_y)
{
	int i, j, interruption_flag = 0;
	for(i = 0; i < 7; i++)
	{
		for(j = 0; j < 7; j++)
		{
			if(floor(chess[i*8+j]/9.0) != floor(chess[attacking_x*8+attacking_y]/9.0) && chess[i*8+j] != chess[king_x*8+king_y] && chess[i*8+j] >= 0)
			{
				behaviour(chess[i*8+j]%9,i,j,chess);
				behaviour(chess[attacking_x*8+attacking_y]%9,attacking_x,attacking_y,chess);
				if(chess[king_x*8+king_y] < 100)
				{
					interruption_flag = 1;
				}
				BoardClear(chess);
			}
		}
	}
	return interruption_flag;
}

int Capture(int attacking_x, int attacking_y, int *chess, int king_x, int king_y)
{
	int i, j, capture_flag = 0;
	for(i = 0; i < 8; i++)
	{
		for(j = 0; j < 8; j++)
		{
			if(floor(chess[i*8+j]/9.0) == floor((chess[king_x*8+king_y]%100)/9.0) && chess[i*8+j] != chess[king_x*8+king_y])
			{
				behaviour(chess[i*8+j]%9,i,j,chess);
				if(chess[attacking_x*8+attacking_y] >= 100) capture_flag = 1;
				BoardClear(chess);
			}
		}
	}
	return capture_flag;
}

int KingCapture(int king_x, int king_y, int *chess, int attacking_x, int attacking_y)
{
	int i, j, king_capture = 1, temp;
	behaviour((chess[king_x*8+king_y]%100)%9,king_x,king_y,chess);
	if(chess[attacking_x*8+attacking_y] >= 100)
	{
		temp = chess[attacking_x*8+attacking_y];
		chess[attacking_x*8+attacking_y] = chess[king_x*8+king_y]%100;
		for(i = 0; i < 8; i++)
		{
			for(j = 0; j < 8; j++)
			{
				if(floor(chess[i*8+j]/9.0) == floor(temp%100/9.0))
				{
					behaviour(chess[i*8+j]%9,i,j,chess);
					if(chess[attacking_x*8+attacking_y] >= 100) king_capture = 0;
					BoardClear(chess);
				}
			}
		}
		chess[king_x*8+king_y] = chess[attacking_x*8+attacking_y];
		chess[attacking_x*8+attacking_y] = temp;
	}
	else king_capture = 0;
	return king_capture;
}